﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Windows.Forms;

namespace BirthDate
{
    public partial class Screen : MetroFramework.Forms.MetroForm
    {
        private FileManager fm = new FileManager();
        private List<Data> dat = new List<Data>();
        private int index = 0;

        public Screen()
        {
            InitializeComponent();
        }

        private void Screen_Load(object sender, EventArgs e)
        {
            fm.CreateFile();

            dat = fm.ReadFile();
            fm.OrderData(ref dat);

            fm.PrintData(mtb, ref dat);
            fm.PrintBirths(mtb6, ref dat, ref index);
        }

        private void mb1_Click(object sender, EventArgs e)
        {
            try
            {
                int d = Int32.Parse(mtb3.Text), m = Int32.Parse(mtb4.Text), y = Int32.Parse(mtb5.Text);

                if (String.IsNullOrEmpty(mtb3.Text) || (d < 1 || d > 31))
                {
                    MessageBox.Show("Invalid Day", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                else if (String.IsNullOrEmpty(mtb3.Text) || (m < 1 || m > 12))
                {
                    MessageBox.Show("Invalid Month", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                else if (String.IsNullOrEmpty(mtb3.Text) || y < 0)
                {
                    MessageBox.Show("Invalid Year", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                else
                {
                    fm.WriteFile(mtb1.Text, mtb2.Text, mtb3.Text, mtb4.Text, mtb5.Text);

                    dat.Add(new Data(mtb1.Text, mtb2.Text, d, m, y));

                    mtb1.Text = "";
                    mtb2.Text = "";
                    mtb3.Text = "";
                    mtb4.Text = "";
                    mtb5.Text = "";

                    fm.OrderData(ref dat);
                    fm.PrintData(mtb, ref dat);
                    fm.PrintBirths(mtb6, ref dat, ref index);

                    index++;
                }
            }
            catch(ArgumentNullException a)
            {
                Console.WriteLine(a.Message);
                MessageBox.Show("String Conversion (null)", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            catch (OverflowException o)
            {
                Console.WriteLine(o.Message);
                MessageBox.Show("String Conversion (overflow)", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            catch (FormatException f)
            {
                Console.WriteLine(f.Message);
                MessageBox.Show("String Conversion (format)", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        private void mb2_Click(object sender, EventArgs e)
        {
            if (index < dat.Count - 1)
            {
                index++;
                fm.PrintBirths(mtb6, ref dat, ref index, false);
            }
        }

        private void mb3_Click(object sender, EventArgs e)
        {
            if(index > 0)
            {
                index--;
                fm.PrintBirths(mtb6, ref dat, ref index, false);
            }
        }
    }
}
