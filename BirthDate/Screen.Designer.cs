﻿
namespace BirthDate
{
    partial class Screen
    {
        /// <summary>
        /// Variabile di progettazione necessaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Pulire le risorse in uso.
        /// </summary>
        /// <param name="disposing">ha valore true se le risorse gestite devono essere eliminate, false in caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Codice generato da Progettazione Windows Form

        /// <summary>
        /// Metodo necessario per il supporto della finestra di progettazione. Non modificare
        /// il contenuto del metodo con l'editor di codice.
        /// </summary>
        private void InitializeComponent()
        {
            this.mtb = new MetroFramework.Controls.MetroTextBox();
            this.mp1 = new MetroFramework.Controls.MetroPanel();
            this.mtb1 = new MetroFramework.Controls.MetroTextBox();
            this.ml1 = new MetroFramework.Controls.MetroLabel();
            this.mp2 = new MetroFramework.Controls.MetroPanel();
            this.mtb2 = new MetroFramework.Controls.MetroTextBox();
            this.ml2 = new MetroFramework.Controls.MetroLabel();
            this.mp3 = new MetroFramework.Controls.MetroPanel();
            this.mtb3 = new MetroFramework.Controls.MetroTextBox();
            this.ml3 = new MetroFramework.Controls.MetroLabel();
            this.mp4 = new MetroFramework.Controls.MetroPanel();
            this.mtb4 = new MetroFramework.Controls.MetroTextBox();
            this.ml4 = new MetroFramework.Controls.MetroLabel();
            this.mp5 = new MetroFramework.Controls.MetroPanel();
            this.mtb5 = new MetroFramework.Controls.MetroTextBox();
            this.ml5 = new MetroFramework.Controls.MetroLabel();
            this.mb1 = new MetroFramework.Controls.MetroButton();
            this.mp6 = new MetroFramework.Controls.MetroPanel();
            this.mb3 = new MetroFramework.Controls.MetroButton();
            this.mb2 = new MetroFramework.Controls.MetroButton();
            this.mtb6 = new MetroFramework.Controls.MetroTextBox();
            this.ml6 = new MetroFramework.Controls.MetroLabel();
            this.mp1.SuspendLayout();
            this.mp2.SuspendLayout();
            this.mp3.SuspendLayout();
            this.mp4.SuspendLayout();
            this.mp5.SuspendLayout();
            this.mp6.SuspendLayout();
            this.SuspendLayout();
            // 
            // mtb
            // 
            this.mtb.Location = new System.Drawing.Point(23, 63);
            this.mtb.Multiline = true;
            this.mtb.Name = "mtb";
            this.mtb.ReadOnly = true;
            this.mtb.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.mtb.Size = new System.Drawing.Size(270, 220);
            this.mtb.TabIndex = 0;
            // 
            // mp1
            // 
            this.mp1.Controls.Add(this.mtb1);
            this.mp1.Controls.Add(this.ml1);
            this.mp1.HorizontalScrollbarBarColor = true;
            this.mp1.HorizontalScrollbarHighlightOnWheel = false;
            this.mp1.HorizontalScrollbarSize = 10;
            this.mp1.Location = new System.Drawing.Point(307, 63);
            this.mp1.Name = "mp1";
            this.mp1.Size = new System.Drawing.Size(270, 28);
            this.mp1.TabIndex = 1;
            this.mp1.VerticalScrollbarBarColor = true;
            this.mp1.VerticalScrollbarHighlightOnWheel = false;
            this.mp1.VerticalScrollbarSize = 10;
            // 
            // mtb1
            // 
            this.mtb1.Location = new System.Drawing.Point(72, 3);
            this.mtb1.Name = "mtb1";
            this.mtb1.Size = new System.Drawing.Size(195, 19);
            this.mtb1.TabIndex = 3;
            this.mtb1.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // ml1
            // 
            this.ml1.AutoSize = true;
            this.ml1.Location = new System.Drawing.Point(3, 3);
            this.ml1.Margin = new System.Windows.Forms.Padding(3);
            this.ml1.Name = "ml1";
            this.ml1.Size = new System.Drawing.Size(45, 19);
            this.ml1.TabIndex = 2;
            this.ml1.Text = "Name";
            // 
            // mp2
            // 
            this.mp2.Controls.Add(this.mtb2);
            this.mp2.Controls.Add(this.ml2);
            this.mp2.HorizontalScrollbarBarColor = true;
            this.mp2.HorizontalScrollbarHighlightOnWheel = false;
            this.mp2.HorizontalScrollbarSize = 10;
            this.mp2.Location = new System.Drawing.Point(307, 97);
            this.mp2.Name = "mp2";
            this.mp2.Size = new System.Drawing.Size(270, 28);
            this.mp2.TabIndex = 4;
            this.mp2.VerticalScrollbarBarColor = true;
            this.mp2.VerticalScrollbarHighlightOnWheel = false;
            this.mp2.VerticalScrollbarSize = 10;
            // 
            // mtb2
            // 
            this.mtb2.Location = new System.Drawing.Point(72, 3);
            this.mtb2.Name = "mtb2";
            this.mtb2.Size = new System.Drawing.Size(195, 19);
            this.mtb2.TabIndex = 3;
            this.mtb2.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // ml2
            // 
            this.ml2.AutoSize = true;
            this.ml2.Location = new System.Drawing.Point(3, 3);
            this.ml2.Margin = new System.Windows.Forms.Padding(3);
            this.ml2.Name = "ml2";
            this.ml2.Size = new System.Drawing.Size(61, 19);
            this.ml2.TabIndex = 2;
            this.ml2.Text = "Surname";
            // 
            // mp3
            // 
            this.mp3.Controls.Add(this.mtb3);
            this.mp3.Controls.Add(this.ml3);
            this.mp3.HorizontalScrollbarBarColor = true;
            this.mp3.HorizontalScrollbarHighlightOnWheel = false;
            this.mp3.HorizontalScrollbarSize = 10;
            this.mp3.Location = new System.Drawing.Point(307, 131);
            this.mp3.Name = "mp3";
            this.mp3.Size = new System.Drawing.Size(270, 28);
            this.mp3.TabIndex = 4;
            this.mp3.VerticalScrollbarBarColor = true;
            this.mp3.VerticalScrollbarHighlightOnWheel = false;
            this.mp3.VerticalScrollbarSize = 10;
            // 
            // mtb3
            // 
            this.mtb3.Location = new System.Drawing.Point(72, 3);
            this.mtb3.Name = "mtb3";
            this.mtb3.Size = new System.Drawing.Size(195, 19);
            this.mtb3.TabIndex = 3;
            this.mtb3.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // ml3
            // 
            this.ml3.AutoSize = true;
            this.ml3.Location = new System.Drawing.Point(3, 3);
            this.ml3.Margin = new System.Windows.Forms.Padding(3);
            this.ml3.Name = "ml3";
            this.ml3.Size = new System.Drawing.Size(31, 19);
            this.ml3.TabIndex = 2;
            this.ml3.Text = "Day";
            // 
            // mp4
            // 
            this.mp4.Controls.Add(this.mtb4);
            this.mp4.Controls.Add(this.ml4);
            this.mp4.HorizontalScrollbarBarColor = true;
            this.mp4.HorizontalScrollbarHighlightOnWheel = false;
            this.mp4.HorizontalScrollbarSize = 10;
            this.mp4.Location = new System.Drawing.Point(307, 165);
            this.mp4.Name = "mp4";
            this.mp4.Size = new System.Drawing.Size(270, 28);
            this.mp4.TabIndex = 4;
            this.mp4.VerticalScrollbarBarColor = true;
            this.mp4.VerticalScrollbarHighlightOnWheel = false;
            this.mp4.VerticalScrollbarSize = 10;
            // 
            // mtb4
            // 
            this.mtb4.Location = new System.Drawing.Point(72, 3);
            this.mtb4.Name = "mtb4";
            this.mtb4.Size = new System.Drawing.Size(195, 19);
            this.mtb4.TabIndex = 3;
            this.mtb4.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // ml4
            // 
            this.ml4.AutoSize = true;
            this.ml4.Location = new System.Drawing.Point(3, 3);
            this.ml4.Margin = new System.Windows.Forms.Padding(3);
            this.ml4.Name = "ml4";
            this.ml4.Size = new System.Drawing.Size(47, 19);
            this.ml4.TabIndex = 2;
            this.ml4.Text = "Month";
            // 
            // mp5
            // 
            this.mp5.Controls.Add(this.mtb5);
            this.mp5.Controls.Add(this.ml5);
            this.mp5.HorizontalScrollbarBarColor = true;
            this.mp5.HorizontalScrollbarHighlightOnWheel = false;
            this.mp5.HorizontalScrollbarSize = 10;
            this.mp5.Location = new System.Drawing.Point(307, 199);
            this.mp5.Name = "mp5";
            this.mp5.Size = new System.Drawing.Size(270, 28);
            this.mp5.TabIndex = 4;
            this.mp5.VerticalScrollbarBarColor = true;
            this.mp5.VerticalScrollbarHighlightOnWheel = false;
            this.mp5.VerticalScrollbarSize = 10;
            // 
            // mtb5
            // 
            this.mtb5.Location = new System.Drawing.Point(72, 3);
            this.mtb5.Name = "mtb5";
            this.mtb5.Size = new System.Drawing.Size(195, 19);
            this.mtb5.TabIndex = 3;
            this.mtb5.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // ml5
            // 
            this.ml5.AutoSize = true;
            this.ml5.Location = new System.Drawing.Point(3, 3);
            this.ml5.Margin = new System.Windows.Forms.Padding(3);
            this.ml5.Name = "ml5";
            this.ml5.Size = new System.Drawing.Size(34, 19);
            this.ml5.TabIndex = 2;
            this.ml5.Text = "Year";
            // 
            // mb1
            // 
            this.mb1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.mb1.Location = new System.Drawing.Point(307, 233);
            this.mb1.Name = "mb1";
            this.mb1.Size = new System.Drawing.Size(270, 50);
            this.mb1.TabIndex = 5;
            this.mb1.Text = "Add Birth";
            this.mb1.Click += new System.EventHandler(this.mb1_Click);
            // 
            // mp6
            // 
            this.mp6.Controls.Add(this.mb3);
            this.mp6.Controls.Add(this.mb2);
            this.mp6.Controls.Add(this.mtb6);
            this.mp6.Controls.Add(this.ml6);
            this.mp6.HorizontalScrollbarBarColor = true;
            this.mp6.HorizontalScrollbarHighlightOnWheel = false;
            this.mp6.HorizontalScrollbarSize = 10;
            this.mp6.Location = new System.Drawing.Point(23, 289);
            this.mp6.Name = "mp6";
            this.mp6.Size = new System.Drawing.Size(554, 31);
            this.mp6.TabIndex = 6;
            this.mp6.VerticalScrollbarBarColor = true;
            this.mp6.VerticalScrollbarHighlightOnWheel = false;
            this.mp6.VerticalScrollbarSize = 10;
            // 
            // mb3
            // 
            this.mb3.Cursor = System.Windows.Forms.Cursors.Hand;
            this.mb3.Location = new System.Drawing.Point(526, 3);
            this.mb3.Name = "mb3";
            this.mb3.Size = new System.Drawing.Size(25, 25);
            this.mb3.TabIndex = 10;
            this.mb3.Text = ">";
            this.mb3.Click += new System.EventHandler(this.mb2_Click);
            // 
            // mb2
            // 
            this.mb2.Cursor = System.Windows.Forms.Cursors.Hand;
            this.mb2.Location = new System.Drawing.Point(124, 3);
            this.mb2.Name = "mb2";
            this.mb2.Size = new System.Drawing.Size(25, 25);
            this.mb2.TabIndex = 9;
            this.mb2.Text = "<";
            this.mb2.Click += new System.EventHandler(this.mb3_Click);
            // 
            // mtb6
            // 
            this.mtb6.FontSize = MetroFramework.MetroTextBoxSize.Medium;
            this.mtb6.Location = new System.Drawing.Point(155, 3);
            this.mtb6.Name = "mtb6";
            this.mtb6.ReadOnly = true;
            this.mtb6.Size = new System.Drawing.Size(365, 25);
            this.mtb6.TabIndex = 3;
            this.mtb6.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // ml6
            // 
            this.ml6.AutoSize = true;
            this.ml6.FontSize = MetroFramework.MetroLabelSize.Tall;
            this.ml6.Location = new System.Drawing.Point(3, 3);
            this.ml6.Margin = new System.Windows.Forms.Padding(3);
            this.ml6.Name = "ml6";
            this.ml6.Size = new System.Drawing.Size(115, 25);
            this.ml6.TabIndex = 2;
            this.ml6.Text = "Next Birthday";
            // 
            // Screen
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(600, 337);
            this.Controls.Add(this.mp6);
            this.Controls.Add(this.mb1);
            this.Controls.Add(this.mp5);
            this.Controls.Add(this.mp4);
            this.Controls.Add(this.mp3);
            this.Controls.Add(this.mp2);
            this.Controls.Add(this.mp1);
            this.Controls.Add(this.mtb);
            this.Name = "Screen";
            this.Resizable = false;
            this.ShadowType = MetroFramework.Forms.MetroForm.MetroFormShadowType.SystemShadow;
            this.Text = "BirthDate";
            this.TextAlign = System.Windows.Forms.VisualStyles.HorizontalAlign.Center;
            this.Load += new System.EventHandler(this.Screen_Load);
            this.mp1.ResumeLayout(false);
            this.mp1.PerformLayout();
            this.mp2.ResumeLayout(false);
            this.mp2.PerformLayout();
            this.mp3.ResumeLayout(false);
            this.mp3.PerformLayout();
            this.mp4.ResumeLayout(false);
            this.mp4.PerformLayout();
            this.mp5.ResumeLayout(false);
            this.mp5.PerformLayout();
            this.mp6.ResumeLayout(false);
            this.mp6.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private MetroFramework.Controls.MetroTextBox mtb;
        private MetroFramework.Controls.MetroPanel mp1;
        private MetroFramework.Controls.MetroTextBox mtb1;
        private MetroFramework.Controls.MetroLabel ml1;
        private MetroFramework.Controls.MetroPanel mp2;
        private MetroFramework.Controls.MetroTextBox mtb2;
        private MetroFramework.Controls.MetroLabel ml2;
        private MetroFramework.Controls.MetroPanel mp3;
        private MetroFramework.Controls.MetroTextBox mtb3;
        private MetroFramework.Controls.MetroLabel ml3;
        private MetroFramework.Controls.MetroPanel mp4;
        private MetroFramework.Controls.MetroTextBox mtb4;
        private MetroFramework.Controls.MetroLabel ml4;
        private MetroFramework.Controls.MetroPanel mp5;
        private MetroFramework.Controls.MetroTextBox mtb5;
        private MetroFramework.Controls.MetroLabel ml5;
        private MetroFramework.Controls.MetroButton mb1;
        private MetroFramework.Controls.MetroPanel mp6;
        private MetroFramework.Controls.MetroTextBox mtb6;
        private MetroFramework.Controls.MetroLabel ml6;
        private MetroFramework.Controls.MetroButton mb3;
        private MetroFramework.Controls.MetroButton mb2;
    }
}

