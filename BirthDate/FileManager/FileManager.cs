﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using MetroFramework.Controls;

namespace BirthDate
{
    class FileManager
    {
        private string path = @"Dates\dates.dat";
        private string dir = @"Dates";

        public void CreateFile()
        {
            if (!Directory.Exists(dir))
            {
                Directory.CreateDirectory(dir);
                var f = File.Create(path);
                f.Close();
            }

            if(!File.Exists(path))
            {
                var f = File.Create(path);
                f.Close();
            }
        }

        public List<Data> ReadFile()
        {
            List<Data> d = new List<Data>();

            using (StreamReader sr = File.OpenText(path))
            {
                string s = ""; string[] line;

                while ((s = sr.ReadLine()) != null)
                {
                    line = s.Split(',');

                    d.Add(new Data(line[0], line[1], Int32.Parse(line[2]), Int32.Parse(line[3]), Int32.Parse(line[4])));
                }
            }

            return d;
        }

        public void WriteFile(string s1, string s2, string s3, string s4, string s5)
        {
            using (StreamWriter sw = File.AppendText(path))
            {
                sw.WriteLine(s1 + "," + s2 + "," + s3 + "," + s4 + "," + s5);
            }
        }

        public void OrderData(ref List<Data> dat)
        {
            DateTime dt = DateTime.Now;

            // Prendi finchè: (1)(md.month == dt.Month && md.day < dt.Day) || (2)md.month < dt.Month
            // (1) il mese uguale all'attuale deve avere il giorno minore dell'attuale
            // (2) il mese deve essere minore del mese attuale
            List<Data> low = dat.OrderBy(m => m.month).ThenBy(d => d.day).TakeWhile(md => (md.month == dt.Month && md.day < dt.Day) || md.month < dt.Month).ToList();
            //checkList(ref low);

            dat = dat.OrderBy(m => m.month).ThenBy(d => d.day).ToList();
            //checkList(ref dat);

            dat.RemoveRange(0, low.Count());
            //checkList(ref dat);

            dat.AddRange(low);
            //checkList(ref dat);
        }

        public void PrintData(MetroTextBox mtb, ref List<Data> dat)
        {
            mtb.Text = "";
            foreach (Data d in dat)
            {
                mtb.AppendText(d.name + " " + d.surname + "\t" + d.DayCorrection(d.day) + "/" + d.MonthCorrection(d.month) + "/" + d.year);
                mtb.AppendText(Environment.NewLine);
            }
        }

        public void PrintBirths(MetroTextBox mtb, ref List<Data> dat, ref int index, bool group = true)
        {
            Console.WriteLine(index);
            if (dat.Count > 0)
            {
                mtb.Text = "| " + dat[index].name + " " + (DateTime.Now.Year - dat[index].year + (dat[index].month < dat[0].month ? 1 : 0)) + " years | ";
            }

            if(group)
            {
                for (int i = index + 1; i < dat.Count; i++)
                {
                    if (dat[i].day == dat[index].day && dat[i].month == dat[index].month)
                    {
                        mtb.Text += dat[i].name + " " + (DateTime.Now.Year - dat[i].year) + " years | ";
                    }
                    else break;
                }
            }
        }

        private void checkList(ref List<Data> dat)
        {
            foreach(Data d in dat)
            {
                Console.WriteLine(d);
            }
            Console.WriteLine("-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.-.");
        }
    }
}
