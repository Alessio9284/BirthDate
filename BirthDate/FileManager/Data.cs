﻿namespace BirthDate
{
    class Data
    {
        public string name, surname;
        public int day, month, year;

        public Data(string name, string surname, int day, int month, int year)
        {
            this.name = name;
            this.surname = surname;
            this.day = day;
            this.month = month;
            this.year = year;
        }

        public string DayCorrection(int day)
        {
            return day < 10 ? "0" + day : "" + day;
        }

        public string MonthCorrection(int month)
        {
            return month < 10 ? "0" + month : "" + month;
        }

        public override string ToString()
        {
            return "{ " + name + ", " + surname + ", " + day + "/" + month + "/" + year + " }";
        }
    }
}
